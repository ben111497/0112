package com.example.user.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main4Activity extends AppCompatActivity
implements View.OnClickListener {
    TextView name2,gender2,phone2;
    Button cancel,redata;
    String name,token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        Intent it= getIntent();
        String realname = it.getStringExtra("realname");
        int gender = it.getIntExtra("gender",0);
        String phone = it.getStringExtra("phone");
        name=it.getStringExtra("name");
        token=it.getStringExtra("token");
        name2=findViewById(R.id.name2);
        gender2=findViewById(R.id.gender2);
        phone2=findViewById(R.id.phone2);
        cancel=findViewById(R.id.cancel);
        redata=findViewById(R.id.redata);
        cancel.setOnClickListener(this);
        redata.setOnClickListener(this);

        try {
            name2.setText("" + realname);
            phone2.setText("" + phone);
            if (gender == 1) {
                gender2.setText("男生");
            } else if (gender == 2) {
                gender2.setText("女生");
            } else {
                gender2.setText("保密!");
            }
        }catch (Exception e){

        }

    }

    @Override
    public void onClick(View v) {
     switch(v.getId()){
         case R.id.cancel:
             Intent i = new Intent();
             i.putExtra("name",name);
             i.putExtra("token",token);
             i.setClass(Main4Activity.this , Main3Activity.class);
             startActivityForResult(i,0);
             break;
         case R.id.redata:
             break;
     }
    }
}
