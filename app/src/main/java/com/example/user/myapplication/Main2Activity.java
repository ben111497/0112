package com.example.user.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class Main2Activity extends AppCompatActivity
implements View.OnClickListener {
    public  class object{
        /**
         * data : {"GetTemporarilyToken":{"Status":{"StatusCode":3,"Description":"Success Get Temporarily Token."},"Token":"849460","GetTimes":"2019-01-22 15:19:53.148613868 +0000 UTC"}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * GetTemporarilyToken : {"Status":{"StatusCode":3,"Description":"Success Get Temporarily Token."},"Token":"849460","GetTimes":"2019-01-22 15:19:53.148613868 +0000 UTC"}
             */

            private GetTemporarilyTokenBean GetTemporarilyToken;

            public GetTemporarilyTokenBean getGetTemporarilyToken() {
                return GetTemporarilyToken;
            }

            public void setGetTemporarilyToken(GetTemporarilyTokenBean GetTemporarilyToken) {
                this.GetTemporarilyToken = GetTemporarilyToken;
            }

            public  class GetTemporarilyTokenBean {
                /**
                 * Status : {"StatusCode":3,"Description":"Success Get Temporarily Token."}
                 * Token : 849460
                 * GetTimes : 2019-01-22 15:19:53.148613868 +0000 UTC
                 */

                private StatusBean Status;
                private String Token;
                private String GetTimes;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public String getToken() {
                    return Token;
                }

                public void setToken(String Token) {
                    this.Token = Token;
                }

                public String getGetTimes() {
                    return GetTimes;
                }

                public void setGetTimes(String GetTimes) {
                    this.GetTimes = GetTimes;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 3
                     * Description : Success Get Temporarily Token.
                     */

                    private int StatusCode;
                    private String Description;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }

                    public String getDescription() {
                        return Description;
                    }

                    public void setDescription(String Description) {
                        this.Description = Description;
                    }
                }
            }
        }
    }

    public class object2{

        /**
         * data : {"AddCarID":{"Status":{"StatusCode":4,"Description":"Success Add Car ID to User."},"ID":"878@gmail.com","CarID":"MAS999","Token":"38373840676d61696c2e636f6d323031392d30312d32322031353a33383a33352e323039313035323332202b3030303020555443206d3d2b3531373536322e333436393332393738333438313539313930322e313438363931313737345b88350753f763d1aa556e0c45246443d755ee81e06fede9fff4e7ef9e3a897ef597e2ceb44a0b1eabd813d7c9072ee07cce061188b6bbf52b8eece1f91773e5"}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * AddCarID : {"Status":{"StatusCode":4,"Description":"Success Add Car ID to User."},"ID":"878@gmail.com","CarID":"MAS999","Token":"38373840676d61696c2e636f6d323031392d30312d32322031353a33383a33352e323039313035323332202b3030303020555443206d3d2b3531373536322e333436393332393738333438313539313930322e313438363931313737345b88350753f763d1aa556e0c45246443d755ee81e06fede9fff4e7ef9e3a897ef597e2ceb44a0b1eabd813d7c9072ee07cce061188b6bbf52b8eece1f91773e5"}
             */

            private AddCarIDBean AddCarID;

            public AddCarIDBean getAddCarID() {
                return AddCarID;
            }

            public void setAddCarID(AddCarIDBean AddCarID) {
                this.AddCarID = AddCarID;
            }

            public  class AddCarIDBean {
                /**
                 * Status : {"StatusCode":4,"Description":"Success Add Car ID to User."}
                 * ID : 878@gmail.com
                 * CarID : MAS999
                 * Token : 38373840676d61696c2e636f6d323031392d30312d32322031353a33383a33352e323039313035323332202b3030303020555443206d3d2b3531373536322e333436393332393738333438313539313930322e313438363931313737345b88350753f763d1aa556e0c45246443d755ee81e06fede9fff4e7ef9e3a897ef597e2ceb44a0b1eabd813d7c9072ee07cce061188b6bbf52b8eece1f91773e5
                 */

                private StatusBean Status;
                private String ID;
                private String CarID;
                private String Token;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public String getID() {
                    return ID;
                }

                public void setID(String ID) {
                    this.ID = ID;
                }

                public String getCarID() {
                    return CarID;
                }

                public void setCarID(String CarID) {
                    this.CarID = CarID;
                }

                public String getToken() {
                    return Token;
                }

                public void setToken(String Token) {
                    this.Token = Token;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 4
                     * Description : Success Add Car ID to User.
                     */

                    private int StatusCode;
                    private String Description;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }

                    public String getDescription() {
                        return Description;
                    }

                    public void setDescription(String Description) {
                        this.Description = Description;
                    }
                }
            }
        }
    }


    String name,token,pro_token;
    Button ok,cancel,newtoken;
    EditText newcarid,newcarname,test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ok=findViewById(R.id.ok);
        ok.setOnClickListener(this);
        cancel=findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        newtoken=findViewById(R.id.newtoken);
        newtoken.setOnClickListener(this);

        newcarid=findViewById(R.id.newcarid);
        newcarname=findViewById(R.id.newcarname);
        test=findViewById(R.id.test);
        Intent it= getIntent();
        name=it.getStringExtra("name");
        token=it.getStringExtra("token");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                AlertDialog.Builder logout=new AlertDialog.Builder(this);
                logout.setMessage("按確定將取消新增返回控制畫面!");
                logout.setTitle("確定要取消新增車輛?");
                logout.setIcon(android.R.drawable.btn_star_big_on);
                logout.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent();
                        i.putExtra("name",name);
                        i.putExtra("token",token);
                        i.setClass(Main2Activity.this , Main3Activity.class);
                        startActivityForResult(i,0);
                    }
                });
                logout.setPositiveButton("繼續", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                logout.show();
                break;
            case R.id.newtoken:
                String url=" https://testapi.dennysora.com:8081/query";
                OkHttpClient client = new OkHttpClient();
                // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
                RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query GetTemporarilyToken {\\n GetTemporarilyToken(\\n ID:\\\""+ name +"\\\",\\n Token:\\\""+ token +"\\\"\\n ){\\n Status{\\n StatusCode\\n Description\\n }\\n Token\\n GetTimes\\n }\\n\\n}\"}");
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Call call = client.newCall(request);
                call.enqueue(new Callback()
                {

                    @Override
                    public void onFailure(Request request, IOException e)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Main2Activity.this,"連線失敗!!",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    @Override
                    public void onResponse(Response response) throws IOException
                    {
                        if(response.isSuccessful()){
                            final String myResponse = response.body().string();
                            Main2Activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String json=myResponse.toString();
                                    Gson gson=new Gson();
                                    object obj=gson.fromJson(json,object.class);
                                    pro_token=obj.getData().getGetTemporarilyToken().getToken();
                                    test.setText(""+pro_token);
                                    }
                            });
                        }
                    }
                });
                break;
            case R.id.ok:
                if(newcarid.getText().toString().length()!=0 && newcarname.getText().toString().length()!=0) {
                    String url2 = " https://testapi.dennysora.com:8081/query";
                    OkHttpClient client2 = new OkHttpClient();
                    // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
                    RequestBody body2 = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), "{\"query\":\"mutation AddCarID {\\n AddCarID(\\n   Token:{Token:\\\"" + token + "\\\"},\\n   InputCarNews:{\\n     ID:\\\"" + name + "\\\",\\n     CarID:\\\"" + newcarid.getText().toString() + "\\\",\\n     CarName:\\\"" + newcarname.getText().toString() + "\\\",\\n     TemporarilyToken:\\\"" + pro_token + "\\\"\\n   }\\n ){\\n  Status{\\n     StatusCode\\n     Description\\n   }\\n   ID\\n   CarID\\n   Token\\n }\\n}\\n\"}");
                    Request request2 = new Request.Builder().url(url2).post(body2).build();
                    Call call2 = client2.newCall(request2);
                    call2.enqueue(new Callback() {

                        @Override
                        public void onFailure(Request request, IOException e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(Main2Activity.this, "連線失敗!!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                final String myResponse = response.body().string();
                                Main2Activity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String json = myResponse.toString();
                                        Gson gson = new Gson();
                                        final object2 obj2 = gson.fromJson(json, object2.class);
                                        AlertDialog.Builder logout2 = new AlertDialog.Builder(Main2Activity.this);
                                        if (obj2.getData().getAddCarID().getStatus().getStatusCode() == 4) {
                                            logout2.setMessage("註冊新車輛成功!/按確定回到控制畫面");
                                            logout2.setTitle("恭喜!");
                                            logout2.setIcon(android.R.drawable.btn_star_big_on);
                                            logout2.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent();
                                                    i.putExtra("name",name);
                                                    i.putExtra("token",token);
                                                    i.putExtra("car_id", obj2.getData().getAddCarID().getCarID());
                                                    i.setClass(Main2Activity.this, Main3Activity.class);
                                                    startActivityForResult(i, 0);
                                                }
                                            });
                                            logout2.show();
                                        } else {
                                            logout2.setMessage("註冊車輛失敗!\n請重新獲取驗證碼");
                                            logout2.setTitle("錯誤!");
                                            logout2.setIcon(android.R.drawable.btn_star_big_on);
                                            logout2.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                            logout2.show();
                                        }

                                    }
                                });
                            }
                        }
                    });
                }
                else if(newcarname.getText().toString().length()==0&&newcarid.getText().toString().length()!=0){
                    Toast.makeText(Main2Activity.this,"請輸入車輛名稱!!",Toast.LENGTH_SHORT).show();
                }
                else if(newcarname.getText().toString().length()!=0&&newcarid.getText().toString().length()==0){
                    Toast.makeText(Main2Activity.this,"請輸入車牌號碼!!",Toast.LENGTH_SHORT).show();
                }
                else if(newcarname.getText().toString().length()==0&&newcarid.getText().toString().length()==0){
                    Toast.makeText(Main2Activity.this,"請輸入資料!!",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
