package com.example.user.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    public  class object{
        /**
         * data : {"LogIn":{"Status":{"StatusCode":2,"Description":"Success LogIn."},"GetTimes":"2019-01-14 16:18:42.894214675 +0000 UTC","AccountID":"abcd@gmail.com","AccountToken":"32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970"}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * LogIn : {"Status":{"StatusCode":2,"Description":"Success LogIn."},"GetTimes":"2019-01-14 16:18:42.894214675 +0000 UTC","AccountID":"abcd@gmail.com","AccountToken":"32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970"}
             */

            private LogInBean LogIn;

            public LogInBean getLogIn() {
                return LogIn;
            }

            public void setLogIn(LogInBean LogIn) {
                this.LogIn = LogIn;
            }

            public  class LogInBean {
                /**
                 * Status : {"StatusCode":2,"Description":"Success LogIn."}
                 * GetTimes : 2019-01-14 16:18:42.894214675 +0000 UTC
                 * AccountID : abcd@gmail.com
                 * AccountToken : 32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970
                 */

                private StatusBean Status;
                private String GetTimes;
                private String AccountID;
                private String AccountToken;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public String getGetTimes() {
                    return GetTimes;
                }

                public void setGetTimes(String GetTimes) {
                    this.GetTimes = GetTimes;
                }

                public String getAccountID() {
                    return AccountID;
                }

                public void setAccountID(String AccountID) {
                    this.AccountID = AccountID;
                }

                public String getAccountToken() {
                    return AccountToken;
                }

                public void setAccountToken(String AccountToken) {
                    this.AccountToken = AccountToken;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 2
                     * Description : Success LogIn.
                     */

                    private int StatusCode;
                    private String Description;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }

                    public String getDescription() {
                        return Description;
                    }

                    public void setDescription(String Description) {
                        this.Description = Description;
                    }
                }
            }
        }
    }
   /*
  public class object {
      data result;
      public class data {
          LogIn result;

          public class LogIn {
              Status result;

              public class Status {
                  private int StatusCode;
                  private String Description;

                  public int getStatusCode() {
                      return StatusCode;
                  }

                  public String getDescription() {
                      return Description;
                  }
              }

              private String GetTimes;
              private String AccountID;
              private String AccountToken;

              public String getGetTimesGet() {
                  return GetTimes;
              }

              public String getAccountID() {
                  return AccountID;
              }

              public String getAccountToken() {
                  return AccountToken;
              }

          }
      }
  }
  */
    /**
     * data : {"LogIn":{"Status":{"StatusCode":2,"Description":"Success LogIn."},"GetTimes":"2019-01-14 16:18:42.894214675 +0000 UTC","AccountID":"abcd@gmail.com","AccountToken":"32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970"}}
     */

    private DataBean data;
    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }


    public static class DataBean {
        /**
         * LogIn : {"Status":{"StatusCode":2,"Description":"Success LogIn."},"GetTimes":"2019-01-14 16:18:42.894214675 +0000 UTC","AccountID":"abcd@gmail.com","AccountToken":"32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970"}
         */

        private LogInBean LogIn;

        public LogInBean getLogIn() {
            return LogIn;
        }

        public void setLogIn(LogInBean LogIn) {
            this.LogIn = LogIn;
        }

        public static class LogInBean {
            /**
             * Status : {"StatusCode":2,"Description":"Success LogIn."}
             * GetTimes : 2019-01-14 16:18:42.894214675 +0000 UTC
             * AccountID : abcd@gmail.com
             * AccountToken : 32383339313235333731313534373438323732326162636440676d61696c2e636f6d333336353030313732302e303236393033363239333d915d28b4a36fecc19c6d58f71e3ce10f2c0187d6279bd43b3510e3cc0d7bf3f905873fc8c8f7753e3a451a53601ece2d435c9c8482bd163e5ea01811945970
             */

            private StatusBean Status;
            private String GetTimes;
            private String AccountID;
            private String AccountToken;

            public StatusBean getStatus() {
                return Status;
            }

            public void setStatus(StatusBean Status) {
                this.Status = Status;
            }

            public String getGetTimes() {
                return GetTimes;
            }

            public void setGetTimes(String GetTimes) {
                this.GetTimes = GetTimes;
            }

            public String getAccountID() {
                return AccountID;
            }

            public void setAccountID(String AccountID) {
                this.AccountID = AccountID;
            }

            public String getAccountToken() {
                return AccountToken;
            }

            public void setAccountToken(String AccountToken) {
                this.AccountToken = AccountToken;
            }

            public static class StatusBean {
                /**
                 * StatusCode : 2
                 * Description : Success LogIn.
                 */

                private int StatusCode;
                private String Description;

                public int getStatusCode() {
                    return StatusCode;
                }

                public void setStatusCode(int StatusCode) {
                    this.StatusCode = StatusCode;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }
            }
        }
    }

    Button in_sign;
    TextView login;
    TextView password,in;
    static Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        in_sign = (Button)findViewById(R.id.in_sign);
        login = (TextView)findViewById(R.id.login);
        password = (TextView)findViewById(R.id.password);
        in_sign.setOnClickListener(this);
        in=findViewById(R.id.in);
        in.setOnClickListener(this);
        //=============================
        /*
        OkHttpClient mOKHttpClient_get = new OkHttpClient();// 網路 get
        final Request request_get = new Request.Builder().url("https://140.124.72.104:18080/").build();
        Call call_get = mOKHttpClient_get.newCall(request_get);
        call_get.enqueue(new Callback()
        {
            @Override
            public void onFailure(Request request, IOException e)
            {
            }
            @Override
            public void onResponse(Response response) throws IOException
            {
                String json = response.body().toString();
            }
        });
            */
        //================================================

        try{
            Intent it= getIntent();
            login.setText(it.getStringExtra("name"));
        }catch (Exception e){
        }
        try{
            Intent i= getIntent();
            login.setText(i.getStringExtra("email"));
        }catch (Exception e){
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.in_sign:
            //網路 post
            String log = login.getText().toString();
            String pass = password.getText().toString();
            String url = " https://testapi.dennysora.com:8081/query";

            OkHttpClient client = new OkHttpClient();
            // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
            RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), "{\"query\":\"query Login{\\n LogIn(ID:\\\"" + log + "\\\",Password:\\\"" + pass + "\\\"){\\n Status{\\n StatusCode\\n Description\\n }\\n GetTimes\\n AccountID\\n AccountToken\\n }\\n}\"}");
            Request request = new Request.Builder().url(url).post(body).build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {

                @Override
                public void onFailure(Request request, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "連線失敗!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        final String myResponse = response.body().string();
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String json = myResponse.toString();
                                Gson gson = new Gson();
                                object obj = gson.fromJson(json, object.class);
                                // int code=obj.getData().getLogIn().getStatus().getStatusCode() ; //轉換錯誤
                                //Toast.makeText(MainActivity.this,"帳號密碼錯誤!",Toast.LENGTH_SHORT).show();
                                if (obj.getData().getLogIn().getStatus().getStatusCode() == 2) {
                                    Toast.makeText(MainActivity.this, "登入成功!", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent();
                                    i.setClass(MainActivity.this, Main3Activity.class);
                                    i.putExtra("name", obj.getData().getLogIn().getAccountID());
                                    i.putExtra("token", obj.getData().getLogIn().getAccountToken());
                                    startActivityForResult(i, 0);
                                } else if (obj.getData().getLogIn().getStatus().getStatusCode() == -4) {
                                    AlertDialog.Builder logout = new AlertDialog.Builder(MainActivity.this);
                                    logout.setMessage("密碼錯誤!");
                                    logout.setTitle("登入失敗");
                                    logout.setIcon(android.R.drawable.btn_star_big_on);
                                    logout.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    logout.show();
                                } else if (obj.getData().getLogIn().getStatus().getStatusCode() == -5) {
                                    AlertDialog.Builder logout = new AlertDialog.Builder(MainActivity.this);
                                    logout.setMessage("帳號不存在!");
                                    logout.setTitle("登入失敗");
                                    logout.setIcon(android.R.drawable.btn_star_big_on);
                                    logout.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    logout.show();
                                } else if (obj.getData().getLogIn().getStatus().getStatusCode() == -3) {
                                    Toast.makeText(MainActivity.this, "請輸入完整帳號!\n帳號為Email", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "請輸入帳號和密碼!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    }
                }
            });
            break;
            case R.id.in:
                Intent i = new Intent();
                i.setClass(MainActivity.this, Main5Activity.class);
                startActivityForResult(i, 0);
                break;
        }
    }

}


