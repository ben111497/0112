package com.example.user.myapplication;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class Main3Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    public class object{

        /**
         * data : {"GetUser":{"Status":{"StatusCode":5,"Description":"Success Get User Data."},"Profile":{"Name":"Ben","Gender":1,"Phone":{"Country":"+886","Number":"92601234"}}}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * GetUser : {"Status":{"StatusCode":5,"Description":"Success Get User Data."},"Profile":{"Name":"Ben","Gender":1,"Phone":{"Country":"+886","Number":"92601234"}}}
             */

            private GetUserBean GetUser;

            public GetUserBean getGetUser() {
                return GetUser;
            }

            public void setGetUser(GetUserBean GetUser) {
                this.GetUser = GetUser;
            }

            public  class GetUserBean {
                /**
                 * Status : {"StatusCode":5,"Description":"Success Get User Data."}
                 * Profile : {"Name":"Ben","Gender":1,"Phone":{"Country":"+886","Number":"92601234"}}
                 */

                private StatusBean Status;
                private ProfileBean Profile;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public ProfileBean getProfile() {
                    return Profile;
                }

                public void setProfile(ProfileBean Profile) {
                    this.Profile = Profile;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 5
                     * Description : Success Get User Data.
                     */

                    private int StatusCode;
                    private String Description;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }

                    public String getDescription() {
                        return Description;
                    }

                    public void setDescription(String Description) {
                        this.Description = Description;
                    }
                }

                public  class ProfileBean {
                    /**
                     * Name : Ben
                     * Gender : 1
                     * Phone : {"Country":"+886","Number":"92601234"}
                     */

                    private String Name;
                    private int Gender;
                    private PhoneBean Phone;

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }

                    public int getGender() {
                        return Gender;
                    }

                    public void setGender(int Gender) {
                        this.Gender = Gender;
                    }

                    public PhoneBean getPhone() {
                        return Phone;
                    }

                    public void setPhone(PhoneBean Phone) {
                        this.Phone = Phone;
                    }

                    public  class PhoneBean {
                        /**
                         * Country : +886
                         * Number : 92601234
                         */

                        private String Country;
                        private String Number;

                        public String getCountry() {
                            return Country;
                        }

                        public void setCountry(String Country) {
                            this.Country = Country;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }
                    }
                }
            }
        }
    }


    Button b1,addcar,b3,b4,person;
    TextView username;
    ProgressBar gas;
    private DrawerLayout draw;
    String name,token;
    //SQLiteDatabase db = null;
    Cursor c=null;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        draw = findViewById(R.id.drawer_layout);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        username=findViewById(R.id.username);
        b1=findViewById(R.id.b1);
        b1.setOnClickListener(this);
        addcar=findViewById(R.id.addcar);
        addcar.setOnClickListener(this);
        b3=findViewById(R.id.b3);
        b3.setOnClickListener(this);
        b4=findViewById(R.id.b4);
        b4.setOnClickListener(this);
        person=findViewById(R.id.person);
        person.setOnClickListener(this);

        gas=findViewById(R.id.gas);
        gas.setProgress(50);

        Intent it= getIntent();
        if(it.getStringExtra("name")!=null&&it.getStringExtra("token")!=null) {
            name = it.getStringExtra("name");
            token = it.getStringExtra("token");

         /*   db = openOrCreateDatabase("databases.db",MODE_PRIVATE,null);
            try {
                String str = "CREATE TABLE table01(_id INTEGER PRIMARY KEY,name TEXT,token TEXT,carid TEXT)";
                db.execSQL(str);
            }catch (Exception e){
            }
            try {
                ContentValues c1 = new ContentValues();
                c1.put("_id", 1);
                c1.put("name", String.valueOf(name));
                c1.put("token", String.valueOf(token));
                //db.update("table01", c1,"where _id = 1",null);
            }catch (Exception e){
                ContentValues c1 = new ContentValues();
                c1.put("_id", 1);
                c1.put("name", String.valueOf(name));
                c1.put("token", String.valueOf(token));
                db.insert("table01", null, c1);
            }
        }
        else{
            try {
                db = openOrCreateDatabase("databases.db", MODE_PRIVATE, null);
                c = db.rawQuery("SELECT * FROM table01 WHERE _id=1", null);
                c.moveToFirst();
                do {
                    name=c.getString(1);
                    token=c.getString(2);
                } while (c.moveToNext());
            }catch (Exception e){
            }*/
        }
        username.setText(""+name);

    }
    /*
    @Override
    protected void onDestroy(){
        super.onDestroy();
        db.execSQL("DROP TABLE table01");
        db.close();
        deleteDatabase("databases.db");
    }*/
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b1:
                break;
            case R.id.addcar:
                Intent i = new Intent();
                i.putExtra("name",name);
                i.putExtra("token",token);
                i.setClass(Main3Activity.this , Main2Activity.class);
                startActivityForResult(i,0);
                break;
            case R.id.b3:
                AlertDialog.Builder logout=new AlertDialog.Builder(this);
                logout.setMessage("確定要登出嗎?");
                logout.setTitle("登出");
                logout.setIcon(android.R.drawable.btn_star_big_on);
                logout.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                logout.setPositiveButton("確定登出", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent();
                        i.putExtra("name",name);
                        i.setClass(Main3Activity.this , MainActivity.class);
                        startActivityForResult(i,0);
                    }
                });
                logout.show();
                break;
            case R.id.b4:
                if(draw.isDrawerOpen(GravityCompat.START)){
                    draw.closeDrawer(GravityCompat.START);
                }else {
                    draw.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.person:
                String url=" https://testapi.dennysora.com:8081/query";
                OkHttpClient client = new OkHttpClient();
                // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
                RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"\\nquery GetUser {\\n GetUser(\\n ID:\\\""+name+"\\\",\\n Token:\\\""+token+"\\\"\\n ){\\n Status{\\n StatusCode\\n Description\\n }\\n Profile{\\n Name\\n Gender\\n Phone{\\n Country\\n Number\\n }\\n }\\n }\\n}\\n\"}");
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Call call = client.newCall(request);
                call.enqueue(new Callback()
                {
                    @Override
                    public void onFailure(Request request, IOException e)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Main3Activity.this,"連線失敗!!",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    @Override
                    public void onResponse(Response response) throws IOException
                    {
                        if(response.isSuccessful()){
                            final String myResponse = response.body().string();
                            Main3Activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String json=myResponse.toString();
                                    Gson gson=new Gson();
                                    object obj=gson.fromJson(json,object.class);
                                    //Toast.makeText(MainActivity.this,"帳號密碼錯誤!",Toast.LENGTH_SHORT).show();
                                    if(obj.getData().getGetUser().getStatus().getStatusCode()==5){
                                        Intent it = new Intent();
                                        it.putExtra("name",name);
                                        it.putExtra("token",token);
                                        it.putExtra("realname",obj.getData().getGetUser().getProfile().getName());
                                        it.putExtra("gender",obj.getData().getGetUser().getProfile().getGender());
                                        it.putExtra("phone",obj.getData().getGetUser().getProfile().getPhone().getCountry()+obj.getData().getGetUser().getProfile().getPhone().getNumber());
                                        it.setClass(Main3Activity.this , Main4Activity.class);
                                        startActivityForResult(it,0);
                                    }else{
                                        Toast.makeText(Main3Activity.this,"TOKEN錯誤!!",Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                        }
                    }
                });

                break;
        }
    }
}
