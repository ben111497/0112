package com.example.user.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class Main5Activity extends AppCompatActivity
implements View.OnClickListener {
    public class object1{

        /**
         * data : {"CheckAccountHas":{"Status":{"StatusCode":6},"Has":false}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * CheckAccountHas : {"Status":{"StatusCode":6},"Has":false}
             */

            private CheckAccountHasBean CheckAccountHas;

            public CheckAccountHasBean getCheckAccountHas() {
                return CheckAccountHas;
            }

            public void setCheckAccountHas(CheckAccountHasBean CheckAccountHas) {
                this.CheckAccountHas = CheckAccountHas;
            }

            public  class CheckAccountHasBean {
                /**
                 * Status : {"StatusCode":6}
                 * Has : false
                 */

                private StatusBean Status;
                private boolean Has;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public boolean isHas() {
                    return Has;
                }

                public void setHas(boolean Has) {
                    this.Has = Has;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 6
                     */

                    private int StatusCode;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }
                }
            }
        }
    }


    public  class object2{

        /**
         * data : {"CreateAccount":{"Status":{"StatusCode":1,"Description":"Success Create Account."},"ID":"676@gmail.com"}}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public  class DataBean {
            /**
             * CreateAccount : {"Status":{"StatusCode":1,"Description":"Success Create Account."},"ID":"676@gmail.com"}
             */

            private CreateAccountBean CreateAccount;

            public CreateAccountBean getCreateAccount() {
                return CreateAccount;
            }

            public void setCreateAccount(CreateAccountBean CreateAccount) {
                this.CreateAccount = CreateAccount;
            }

            public  class CreateAccountBean {
                /**
                 * Status : {"StatusCode":1,"Description":"Success Create Account."}
                 * ID : 676@gmail.com
                 */

                private StatusBean Status;
                private String ID;

                public StatusBean getStatus() {
                    return Status;
                }

                public void setStatus(StatusBean Status) {
                    this.Status = Status;
                }

                public String getID() {
                    return ID;
                }

                public void setID(String ID) {
                    this.ID = ID;
                }

                public  class StatusBean {
                    /**
                     * StatusCode : 1
                     * Description : Success Create Account.
                     */

                    private int StatusCode;
                    private String Description;

                    public int getStatusCode() {
                        return StatusCode;
                    }

                    public void setStatusCode(int StatusCode) {
                        this.StatusCode = StatusCode;
                    }

                    public String getDescription() {
                        return Description;
                    }

                    public void setDescription(String Description) {
                        this.Description = Description;
                    }
                }
            }
        }
    }


        TextView name,email,fphone,bphone,password1,password2;
        Button cancel,log,check;
        RadioGroup gender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        gender=findViewById(R.id.gender);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        fphone=findViewById(R.id.fphone);
        bphone=findViewById(R.id.bphone);
        password1=findViewById(R.id.password1);
        password2=findViewById(R.id.password2);
        cancel=findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        log=findViewById(R.id.log);
        log.setOnClickListener(this);
        check=findViewById(R.id.check);
        check.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel:
                AlertDialog.Builder logout = new AlertDialog.Builder(Main5Activity.this);
                logout.setMessage("按確定離開此頁面\n以輸入資料不會保留!");
                logout.setTitle("確定要取消註冊?");
                logout.setIcon(android.R.drawable.btn_star_big_on);
                logout.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent();
                        i.setClass(Main5Activity.this, MainActivity.class);
                        startActivityForResult(i, 0);
                    }
                });
                logout.setNegativeButton("繼續", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                logout.show();

                break;
            case R.id.check:
                String url=" https://testapi.dennysora.com:8081/query";
                OkHttpClient client = new OkHttpClient();
                // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
                RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query Check{\\n  CheckAccountHas(ID:\\\""+email.getText().toString()+"\\\"){\\n  Status{\\n    StatusCode\\n    StatusCode\\n  }\\n  Has\\n  }\\n}\\n\"}");
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Call call = client.newCall(request);
                call.enqueue(new Callback()
                {

                    @Override
                    public void onFailure(Request request, IOException e)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Main5Activity.this,"連線失敗!!",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    @Override
                    public void onResponse(Response response) throws IOException
                    {
                        if(response.isSuccessful()){
                            final String myResponse = response.body().string();
                            Main5Activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String json=myResponse.toString();
                                    Gson gson=new Gson();
                                    object1 obj=gson.fromJson(json,object1.class);
                                  if(obj.getData().getCheckAccountHas().isHas()==true){
                                      Toast.makeText(Main5Activity.this,"錯誤!:此信箱已經使用過!!\n請重新輸入",Toast.LENGTH_SHORT).show();
                                  }
                                  else if(obj.getData().getCheckAccountHas().getStatus().getStatusCode()==-3){
                                      Toast.makeText(Main5Activity.this,"錯誤!\n請使用電子信箱格式!!",Toast.LENGTH_SHORT).show();
                                  }
                                  else if(obj.getData().getCheckAccountHas().getStatus().getStatusCode()==-1){
                                      Toast.makeText(Main5Activity.this,"錯誤!\n請輸入電子信箱!!",Toast.LENGTH_SHORT).show();
                                  }
                                  else if(obj.getData().getCheckAccountHas().isHas()==false){
                                    Toast.makeText(Main5Activity.this,"恭喜!\n此信箱可以使用!!",Toast.LENGTH_SHORT).show();
                                  }
                                  else{
                                      Toast.makeText(Main5Activity.this,"系統錯誤",Toast.LENGTH_SHORT).show();
                                  }
                                }
                            });
                        }
                    }
                });
                break;
            case R.id.log:
                    if (name.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請輸入名稱!!", Toast.LENGTH_SHORT).show();
                    } else if (email.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請輸入信箱!!", Toast.LENGTH_SHORT).show();
                    } else if (fphone.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請輸入國碼!!", Toast.LENGTH_SHORT).show();
                    } else if (bphone.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請輸入電話號碼!!", Toast.LENGTH_SHORT).show();
                    } else if (password1.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請輸入密碼!!", Toast.LENGTH_SHORT).show();
                    } else if (password2.getText().length() == 0) {
                        Toast.makeText(Main5Activity.this, "錯誤:請再次輸入密碼!!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!password1.getText().toString().equals(password2.getText().toString())) {
                            Toast.makeText(Main5Activity.this, "錯誤:兩組密碼不相同!!", Toast.LENGTH_SHORT).show();
                        } else {
                            int gg = 0;
                            switch (gender.getCheckedRadioButtonId()) {
                                case R.id.boy:
                                    gg = 1;
                                    break;
                                case R.id.girl:
                                    gg = 2;
                                    break;
                                case R.id.nn:
                                    gg = 3;
                                    break;
                            }
                            String url2 = " https://testapi.dennysora.com:8081/query";
                            OkHttpClient client2 = new OkHttpClient();
                            // RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),"{\"query\":\"query LogIn{\\n LogIn(ID:\\\""+log+"\\\",Password:\\\""+pass+"\\\"){\\n        Status{\\n     StatusCode\\n     Description\\n   }\\n   GetTimes\\n   AccountID\\n   AccountToken\\n }\\n}\"}");
                            RequestBody body2 = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), "{\"query\":\"mutation Create{\\n CreateAccount (\\n   AccountIDPW: {\\n     Account:\\\""+email.getText().toString()+"\\\",\\n     Password:\\\""+password1.getText().toString()+"\\\"\\n   },\\n   User: {\\n     Name: \\\""+name.getText().toString()+"\\\",\\n     Gender: "+gg+",\\n     Country: \\\""+"+"+fphone.getText().toString()+"\\\",\\n     Number:\\\""+bphone.getText().toString()+"\\\"\\n   }){\\n   Status{\\n     StatusCode\\n     Description\\n   }\\n   ID\\n }\\n}\"}");
                            Request request2 = new Request.Builder().url(url2).post(body2).build();
                            Call call2 = client2.newCall(request2);
                            call2.enqueue(new Callback() {

                                @Override
                                public void onFailure(Request request, IOException e) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(Main5Activity.this, "連線失敗!!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                @Override
                                public void onResponse(Response response) throws IOException {
                                    if (response.isSuccessful()) {
                                        final String myResponse = response.body().string();
                                        Main5Activity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                String json = myResponse.toString();
                                                Gson gson = new Gson();
                                                object2 obj2 = gson.fromJson(json, object2.class);
                                                if(obj2.getData().getCreateAccount().getStatus().getStatusCode()==1){
                                                    AlertDialog.Builder logout = new AlertDialog.Builder(Main5Activity.this);
                                                    logout.setMessage("註冊成功!\n按確定離開此頁面");
                                                    logout.setTitle("恭喜!");
                                                    logout.setIcon(android.R.drawable.btn_star_big_on);
                                                    logout.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent i = new Intent();
                                                            i.putExtra("email",email.getText().toString());
                                                            i.setClass(Main5Activity.this, MainActivity.class);
                                                            startActivityForResult(i, 0);
                                                        }
                                                    });
                                                    logout.show();
                                                }
                                                else{
                                                    Toast.makeText(Main5Activity.this, "註冊失敗!!\n請確認信箱是否重複", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                break;
        }
    }
}
